const kOpenWeatherMapApiUrl = 'https://api.openweathermap.org/data/2.5/';
const kOpenWeatherMapBaseUrl = 'api.openweathermap.org';
const kOpenWeatherImageUrl = 'http://openweathermap.org/img/wn/';
const kOpenWeatherImageFormat = '@2x.png';
const kForecastApiExclude = 'current,minutely,alerts';
const kForecastApiUnits = 'metric';
const kForecastLocalKeyPrefix = 'forecast_';
const kLocationLocalKeyPrefix = 'location';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'forecast_entity.freezed.dart';

part 'forecast_entity.g.dart';

@freezed
class ForecastEntity with _$ForecastEntity {
  @JsonSerializable(explicitToJson: true)
  const factory ForecastEntity(
    @JsonKey(name: 'hourly') List<HourlyForecastEntity> hourly,
    @JsonKey(name: 'daily') List<DailyForecastEntity> daily,
  ) = _ForecastEntity;

  factory ForecastEntity.fromJson(Map<String, dynamic> json) =>
      _$ForecastEntityFromJson(json);
}

@freezed
class HourlyForecastEntity with _$HourlyForecastEntity {
  factory HourlyForecastEntity(
    @JsonKey(name: 'dt') int datetime,
    @JsonKey(name: 'temp') double temperature,
    @JsonKey(name: 'weather') List<ForecastWeatherEntity> weather,
  ) = _HourlyForecastEntity;

  factory HourlyForecastEntity.fromJson(Map<String, dynamic> json) =>
      _$HourlyForecastEntityFromJson(json);
}

@freezed
class ForecastWeatherEntity with _$ForecastWeatherEntity {
  factory ForecastWeatherEntity(
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'icon') String icon,
  ) = _ForecastWeatherEntity;

  factory ForecastWeatherEntity.fromJson(Map<String, dynamic> json) =>
      _$ForecastWeatherEntityFromJson(json);
}

@freezed
class DailyForecastEntity with _$DailyForecastEntity {
  factory DailyForecastEntity(
    @JsonKey(name: 'dt') int datetime,
    @JsonKey(name: 'temp') DailyForecastTemperatureEntity temperature,
    @JsonKey(name: 'weather') List<ForecastWeatherEntity> weather,
  ) = _DailyForecastEntity;

  factory DailyForecastEntity.fromJson(Map<String, dynamic> json) =>
      _$DailyForecastEntityFromJson(json);
}

@freezed
class DailyForecastTemperatureEntity with _$DailyForecastTemperatureEntity {
  factory DailyForecastTemperatureEntity(
    @JsonKey(name: 'day') double dayTemperature,
  ) = _DailyForecastTemperatureEntity;

  factory DailyForecastTemperatureEntity.fromJson(Map<String, dynamic> json) =>
      _$DailyForecastTemperatureEntityFromJson(json);
}

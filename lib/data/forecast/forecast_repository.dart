import 'package:weather/data/forecast/forecast_data_source.dart';
import 'package:weather/data/forecast/forecast_mapper.dart';
import 'package:weather/domain/forecast.dart';

abstract class ForecastRepository {
  Future<Forecast> getForecast({
    required double lat,
    required double lon,
  });
}

class DefaultForecastRepository implements ForecastRepository {
  DefaultForecastRepository({
    required ForecastDataSource localDataSource,
    required ForecastDataSource apiDataSource,
  })  : _localDataSource = localDataSource,
        _apiDataSource = apiDataSource;

  final ForecastDataSource _localDataSource;
  final ForecastDataSource _apiDataSource;

  late final _mapper = ForecastMapper();

  @override
  Future<Forecast> getForecast({
    required double lat,
    required double lon,
  }) async {
    final apiData = await _apiDataSource.getForecast(lat: lat, lon: lon);
    if (apiData != null) {
      return _mapper.mapForecastEntity(apiData);
    }
    throw Exception('Something went wrong');
  }

  void _cacheForecast(Forecast forecast) {}
}

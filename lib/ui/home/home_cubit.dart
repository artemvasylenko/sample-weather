import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/domain/use_case/get_current_location.dart';
import 'package:weather/domain/use_case/get_forecast.dart';
import 'package:weather/ui/home/forecast_mode.dart';
import 'package:weather/ui/home/home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({
    required GetCurrentLocation getCurrentLocation,
    required GetForecast getForecast,
  })  : _getCurrentLocation = getCurrentLocation,
        _getForecast = getForecast,
        super(const HomeState.loading());

  final GetCurrentLocation _getCurrentLocation;
  final GetForecast _getForecast;

  void loadForecast() async {
    final currentLocation = await _getCurrentLocation();
    if (currentLocation == null) {
      // Notify about error, if there is no location
      emit(const HomeState.error());
      return;
    }
    final forecast = await _getForecast(GetForecastParameters(
      latitude: currentLocation.lat,
      longitude: currentLocation.lon,
    ));
    emit(HomeState.loaded(
      forecast: forecast,
      mode: ForecastMode.hourly,
    ));
  }

  void setMode(ForecastMode mode) {
    final finalState = state;
    if (finalState is Loaded) {
      emit(finalState.copyWith(mode: mode));
    }
  }
}

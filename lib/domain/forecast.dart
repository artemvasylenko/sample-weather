import 'package:freezed_annotation/freezed_annotation.dart';

part 'forecast.freezed.dart';

@freezed
class Forecast with _$Forecast {
  const factory Forecast({
    required List<HourlyForecast> hourly,
    required List<DailyForecast> daily,
  }) = _Forecast;
}

@freezed
class HourlyForecast with _$HourlyForecast {
  factory HourlyForecast({
    required String time,
    required double temperature,
    required String imageUrl,
  }) = _HourlyForecast;
}

@freezed
class DailyForecast with _$DailyForecast {
  factory DailyForecast({
    required String date,
    required double temperature,
    required String imageUrl,
  }) = _DailyForecast;
}

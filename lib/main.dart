import 'package:dio/dio.dart';
import 'package:dio_logging_interceptor/dio_logging_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/auth/openweathermap_auth_interceptor.dart';
import 'package:weather/data/current_location/current_location_data_source.dart';
import 'package:weather/data/current_location/current_location_mapper.dart';
import 'package:weather/data/current_location/current_location_repository.dart';
import 'package:weather/data/forecast/forecast_api.dart';
import 'package:weather/data/forecast/forecast_data_source.dart';
import 'package:weather/data/forecast/forecast_repository.dart';
import 'package:weather/data/key_value_data_source.dart';
import 'package:weather/ui/app_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(MultiProvider(
    providers: [
      Provider<Dio>(
        create: (context) => Dio(BaseOptions())
          ..interceptors.add(OpenWeatherMapAuthInterceptor(
            'b0f888a4b3550bcfc8a2d68ba5ea2164',
          ))
          ..interceptors.add(
            DioLoggingInterceptor(
              level: Level.body,
            ),
          ),
      ),
      Provider<KeyValueDataSource>(
        create: (context) => SharedPreferencesDataSource(
          sharedPreferences: sharedPreferences,
        ),
      ),
    ],
    child: MultiRepositoryProvider(
      providers: [
        RepositoryProvider<CurrentLocationRepository>(
          create: (context) => DefaultCurrentLocationRepository(
            localDataSource: CurrentLocationLocalDataSource(
              keyValueDataSource: Provider.of(context, listen: false),
            ),
            mapper: CurrentLocationMapper(),
          ),
        ),
        RepositoryProvider<ForecastRepository>(
          create: (context) => DefaultForecastRepository(
            apiDataSource: ForecastApiDataSource(
              forecastApi: ForecastApi(
                Provider.of(context, listen: false),
              ),
            ),
            localDataSource: ForecastLocalDataSource(
              keyValueDataSource: Provider.of(context, listen: false),
            ),
          ),
        ),
      ],
      child: const AppWidget(),
    ),
  ));
}

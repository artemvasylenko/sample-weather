import 'package:dio/dio.dart';
import 'package:weather/data/constants.dart';

const _kAppIdKey = 'appid';

class OpenWeatherMapAuthInterceptor extends Interceptor {
  OpenWeatherMapAuthInterceptor(
    this.appId,
  );

  // Used to authenticate requests
  final String appId;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (options.baseUrl.contains(kOpenWeatherMapBaseUrl)) {
      // We should add app ID if using 'open weather map'
      final updatedQueryParameters = options.queryParameters;
      updatedQueryParameters[_kAppIdKey] = appId;
      return super.onRequest(
        options.copyWith(queryParameters: updatedQueryParameters),
        handler,
      );
    }
    return super.onRequest(options, handler);
  }
}
